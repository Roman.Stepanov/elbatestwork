﻿using System;
using System.IO;

namespace ElbaTestWork
{
    public static class Constants
    {
        public const string URL = "https://raw.githubusercontent.com/Newbilius/ElbaMobileXamarinDeveloperTest/master/json";
        public static readonly string[] URLS = new string[]
        {
            "/generated-01.json",
            "/generated-02.json",
            "/generated-03.json"
        };

        public const string DatabaseFilename = "ItemsDataBase.db3";

        public const string ErrorAlertTitle = "Что-то пошло не так...";
        public const string UnexpectedError = "Непредвиденная ошибка!";
        public const string CloseButtonText = "Закрыть";

        public const int DBExpireTime = 60; // seconds
        public const string LoadedDatePropertyName = "loadedDate";

        public const SQLite.SQLiteOpenFlags Flags =
            // open the database in read/write mode
            SQLite.SQLiteOpenFlags.ReadWrite |
            // create the database if it doesn't exist
            SQLite.SQLiteOpenFlags.Create |
            // enable multi-threaded database access
            SQLite.SQLiteOpenFlags.SharedCache;

        public static string DatabasePath
        {
            get
            {
                var basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                return Path.Combine(basePath, DatabaseFilename);
            }
        }
    }
}
