﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;

namespace ElbaTestWork.Models
{
    public class Person
    {
        [PrimaryKey, Unique]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public double Height { get; set; }
        public string Biography { get; set; }
        public Temperament Temperament { get; set; }
        [OneToOne]
        public EducationPeriod EducationPeriod { get; set; }
        public string SearchField { get; set; }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}