﻿using System.ComponentModel;

namespace ElbaTestWork.Models
{
    public enum Temperament
    {
        [Description("Не определено")]
        Undefined = 0,

        [Description("Меланхолик")]
        Melancholic = 1,

        [Description("Сангвиник")]
        Sanguine = 2,

        [Description("Холерик")]
        Choleric = 3,

        [Description("Флегматик")]
        Phlegmatic = 4,
    }
}
