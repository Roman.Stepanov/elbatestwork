﻿using SQLite;
using System;
using Newtonsoft.Json;

namespace ElbaTestWork.Models
{
    public class EducationPeriod
    {
        [PrimaryKey, JsonIgnore, Unique]
        public string Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
