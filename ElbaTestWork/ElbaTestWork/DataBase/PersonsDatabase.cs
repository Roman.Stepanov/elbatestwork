﻿using ElbaTestWork.Helpers;
using ElbaTestWork.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace ElbaTestWork.DataBase
{
    public class PersonsDatabase
    {
        private static readonly Lazy<SQLiteAsyncConnection> _lazyInitializer = new Lazy<SQLiteAsyncConnection>(() =>
        {
            return new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        });

        static SQLiteAsyncConnection Database => _lazyInitializer.Value;
        private static bool _initialized = false;

        public PersonsDatabase()
        {
            InitializeAsync().SafeFireAndForget(false);
        }

        private async Task InitializeAsync()
        {
            try
            {
                if (!_initialized)
                {
                    _initialized = Database.TableMappings.Any(m =>
                        m.MappedType.Name == typeof(Person).Name &&
                        m.MappedType.Name == typeof(EducationPeriod).Name);

                    if (!_initialized)
                    {
                        var t1 = Database.CreateTablesAsync(CreateFlags.None, typeof(Person));
                        var t2 = Database.CreateTablesAsync(CreateFlags.None, typeof(EducationPeriod));

                        await Task.WhenAll(t1, t2).ConfigureAwait(false);

                        _initialized = true;
                    }
                }
            }
            catch (SQLite.SQLiteException exception)
            {
                App.ErrorEventInvoke(this, new ErrorEventArgs(exception.Message));
            }
            catch (Exception exception)
            {
                App.ErrorEventInvoke(this, new ErrorEventArgs(Constants.UnexpectedError));
#if DEBUG
                System.Diagnostics.Debug.WriteLine(exception);
#endif
            }

        }

        public Task<List<Person>> GetPersonItemsAsync()
        {
            return Database.Table<Person>().OrderBy(x => x.Name).ToListAsync();
        }

        public Task<List<Person>> GetPersonItemsAsync(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return GetPersonItemsAsync();
            }

            searchText = searchText.ToLower();

            var result = Database.Table<Person>()
                .Where(x => x.SearchField.Contains(searchText))
                .OrderBy(x => x.Name)
                .ToListAsync();

            return result;
        }

        public Task<EducationPeriod> GetPeriodOfPersonAsync(string personId)
        {
            return Database.Table<EducationPeriod>().Where(i => i.Id == personId).FirstOrDefaultAsync();
        }

        public Task<Person> GetPersonItemAsync(string id)
        {
            return Database.Table<Person>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task SavePersonItemsAsync(IEnumerable<Person> personItems)
        {
            var taskClearPersonTable = Database.DeleteAllAsync<Person>();
            var taskClearEducationPeriodTable = Database.DeleteAllAsync<EducationPeriod>();
            await Task.WhenAll(taskClearPersonTable, taskClearEducationPeriodTable);

            personItems.ForEach(person => 
            {
                Regex regexNumbers = new Regex(@"[^\d]");
                var phone = regexNumbers.Replace(person.Phone, "");
                var name = person.Name.ToLower();
                person.SearchField = name + " " + phone;
            });

            var taskAddPersonItems = Database.InsertAllAsync(personItems);
            var taskAddEducationPeriodItems = Database.InsertAllAsync(personItems.Select(x =>
                {
                    x.EducationPeriod.Id = x.Id;
                    return x.EducationPeriod;
                }));
            await Task.WhenAll(taskAddPersonItems, taskAddEducationPeriodItems);

            Application.Current.Properties[Constants.LoadedDatePropertyName] = DateTime.Now.ToString();
        }

        public bool IsExpire()
        {
            if (Application.Current.Properties.ContainsKey(Constants.LoadedDatePropertyName))
            {
                var loadedDateString = Application.Current.Properties[Constants.LoadedDatePropertyName] as string;
                var loadedDate = DateTime.Parse(loadedDateString);
                bool isExpired = loadedDate == null ? true : DateTime.Now > loadedDate.AddSeconds(Constants.DBExpireTime);
                return isExpired;
            }
            return true;
        }
    }
}