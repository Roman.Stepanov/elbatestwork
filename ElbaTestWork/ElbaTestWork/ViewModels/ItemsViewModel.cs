﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using ElbaTestWork.Models;
using Xamarin.Forms.Internals;
using ElbaTestWork.Helpers;

namespace ElbaTestWork.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        private readonly DataHelper _dataHelper;

        public ObservableCollection<Person> Items { get; set; } = new ObservableCollection<Person>();
        public Command LoadItemsCommand { get; set; }
        public string SearchText { get; set; }
        
        bool _isDataUpdating = true;
        public bool IsDataUpdating
        {
            get { return _isDataUpdating; }
            set { SetProperty(ref _isDataUpdating, value); }
        }
        public ItemsViewModel()
        {
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
            _dataHelper = new DataHelper();

        }

        private async Task ExecuteLoadItemsCommand()
        {
            IsBusy = !IsDataUpdating;

            try
            {
                await _dataHelper.GetPersonItemsListAsync();
            }
            finally
            {
                await UpdateItemsList(SearchText);
                IsBusy = false;
                IsDataUpdating = false;
            }
        }

        internal async Task UpdateItemsList(string searchText)
        {
            IsDataUpdating = true;
            IsBusy = false;
            try
            {
                Items.Clear();
                var filteredPersonItems = await _dataHelper.GetFilteredPersonListAsync(searchText);
                filteredPersonItems.ForEach(x => Items.Add(x));

            }
            finally
            {
                IsDataUpdating = false;
            }
        }
    }
}