﻿using ElbaTestWork.Models;


namespace ElbaTestWork.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Person Item { get; set; }
        public ItemDetailViewModel(Person item = null)
        {
            Item = item;
        }

        public void SetEducationPeriod(EducationPeriod educationPeriod)
        {
            if (educationPeriod == null)
                return;

            Item.EducationPeriod = educationPeriod;
            OnPropertyChanged(nameof(Item));
        }
    }
}
