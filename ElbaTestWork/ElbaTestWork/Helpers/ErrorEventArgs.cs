﻿using System;

namespace ElbaTestWork.Helpers
{
    public class ErrorEventArgs : EventArgs
    {
        public string ErrorText { get; set; }

        public ErrorEventArgs(string errorText)
        {
            ErrorText = errorText;
        }
    }
}
