﻿using ElbaTestWork.Models;
using ElbaTestWork.Services;
using Refit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace ElbaTestWork.Helpers
{
    public class DataHelper
    {
        private IHttpClient _httpApiService;

        public DataHelper()
        {
            var refitSettings = new RefitSettings { CollectionFormat = CollectionFormat.Multi };
            _httpApiService = RestService.For<IHttpClient>(Constants.URL, refitSettings);
        }

        public async Task<List<Person>> GetPersonItemsListAsync()
        {
            List<Person> personItems = new List<Person>();

            try
            {
                bool dbIsExpire = App.Database.IsExpire();
                if (!dbIsExpire)
                {
                    return await App.Database.GetPersonItemsAsync();
                }

                var getDataFromServerTasks = Constants.URLS.Select(x => _httpApiService.GetDataFromSource(x));
                await Task.WhenAll(getDataFromServerTasks);

                getDataFromServerTasks.ForEach(task => personItems.AddRange(task.Result));
                personItems = personItems.OrderBy(x => x.Name).Distinct().ToList();
                await App.Database.SavePersonItemsAsync(personItems);
            }
            catch (Refit.ApiException exception)
            {
                App.ErrorEventInvoke(this, new ErrorEventArgs(exception.Message));
            }
            catch (SQLite.SQLiteException exception)
            {
                App.ErrorEventInvoke(this, new ErrorEventArgs(exception.Message));
            }
            catch (Exception exception)
            {
                App.ErrorEventInvoke(this, new ErrorEventArgs(Constants.UnexpectedError));
#if DEBUG
                Debug.WriteLine(exception);
#endif
            }
            finally
            {
                if (personItems.Count == 0)
                    personItems = await App.Database.GetPersonItemsAsync();
            }
            return personItems;
        }

        internal async Task<IEnumerable<Person>> GetFilteredPersonListAsync(string searchText)
        {
            return await App.Database.GetPersonItemsAsync(searchText);
        }
    }
}
