﻿using ElbaTestWork.Models;
using Refit;
using System.Threading.Tasks;

namespace ElbaTestWork.Services
{
    public interface IHttpClient
    {
        [Get("/{url}")]
        Task<Person[]> GetDataFromSource(string url);
    }
}
