﻿using Xamarin.Forms;
using ElbaTestWork.DataBase;
using System;
using ElbaTestWork.Helpers;

namespace ElbaTestWork
{
    public partial class App : Application
    {
        public static event EventHandler<ErrorEventArgs> ErrorEvent;
        public App()
        {
            InitializeComponent();

            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        static PersonsDatabase database;
        public static PersonsDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new PersonsDatabase();
                }
                return database;
            }
        }
        public static void ErrorEventInvoke(object sender, ErrorEventArgs e)
        {
            ErrorEvent.Invoke(sender, e);
        }
    }
}
