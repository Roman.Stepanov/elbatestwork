﻿using ElbaTestWork.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace ElbaTestWork.Converters
{
    class EducationPeriodToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is EducationPeriod educationPeriod)
                return $"{educationPeriod.Start:dd.MM.yyyy} - {educationPeriod.End:dd.MM.yyyy}";

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
