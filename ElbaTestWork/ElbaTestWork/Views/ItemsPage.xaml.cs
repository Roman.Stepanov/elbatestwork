﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;

using ElbaTestWork.Models;
using ElbaTestWork.ViewModels;
using ElbaTestWork.Helpers;

namespace ElbaTestWork.Views
{
    [DesignTimeVisible(false)]
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel viewModel;

        public ItemsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ItemsViewModel();
        }

        async void OnItemSelected(object sender, EventArgs args)
        {
            var layout = (BindableObject)sender;
            var item = (Person)layout.BindingContext;
            await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item)));
        }

        async void OnSearchBarTextChanged(object sender, TextChangedEventArgs e)
        {
            // Задержка, чтобы обновление списка происходило не после каждого ввода символа
            await Task.Delay(500);
            
            if (sender is SearchBar searchBar && e.NewTextValue == searchBar.Text)
                await viewModel.UpdateItemsList(searchBar.Text);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.IsBusy = true;

            App.ErrorEvent += ViewModel_ErrorEvent;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.ErrorEvent -= ViewModel_ErrorEvent;
        }

        private void ViewModel_ErrorEvent(object sender, ErrorEventArgs e)
        {
            DisplayAlert(Constants.ErrorAlertTitle, e.ErrorText, Constants.CloseButtonText);
        }
    }
}