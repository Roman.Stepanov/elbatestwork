﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using ElbaTestWork.ViewModels;
using Xamarin.Essentials;

namespace ElbaTestWork.Views
{
    [DesignTimeVisible(false)]
    public partial class ItemDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;

        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        private void OnPhoneTapped(object sender, EventArgs args)
        {
            if (sender is Label label)
            {
                try
                {
                    PhoneDialer.Open(label.Text);
                }
                catch (ArgumentNullException anEx)
                {
                    // Number was null or white space
                    //DisplayAlert(Constants.ErrorAlertTitle, anEx.Message, Constants.CloseButtonText);
                }
                catch (FeatureNotSupportedException ex)
                {
                    // Phone Dialer is not supported on this device.
                    DisplayAlert(Constants.ErrorAlertTitle, "Телефонный вызов не поддерживается на данном устройстве", Constants.CloseButtonText);
                }
                catch (Exception ex)
                {
                    // Other error has occurred.
                    DisplayAlert(Constants.ErrorAlertTitle, Constants.UnexpectedError, Constants.CloseButtonText);
                }
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var educationPeriod = await App.Database.GetPeriodOfPersonAsync(viewModel.Item.Id);
            viewModel.SetEducationPeriod(educationPeriod);
        }
    }
}